-module(km).
-export([start/0, pool/1, init_agent/4, master/5, print_pforg_table/1]).

-define(UNSATISF_THRESH, 0.1).
-define(MONITCOST, 0.01).
-define(PUNCOST, 0.00).
-define(TURNS_PENALTY, 10). %number of turns sanctioned agent should stay without playing
-define(VERBOSE, false).
-define(GAMEMODE, ddj). %valid values: voices, ddj, pvp
-define(RESOURCESMODE, random).
-define(QUITERS, true).


-record(recsanc, {sanctcount=maps:new(),
                  moncost=maps:new(),
                  puncost=maps:new()
                 }).


%% Create an actor to take care of pool management
pool(Total) ->
    receive
        %% agents may request or contribute resources to the pool
        {request, Amount, RequesterID} ->
            Available = lists:min([Total, Amount]),
            RequesterID ! {pool_resources, Available},
            pool(Total-Available);
        {contribute, Amount} ->
            pool(Total+Amount);
        terminate ->
            aux:check_unread_msgs(),
            exit(normal);
        _Other ->
            erlang:error(io:format("Pool - unrecognized message: ~w~n", [_Other]))
    end.


%% Agent actor. Subject to the actions:
%% new_turn       - define Gi, Qi, Pi, Di and communicate it to Pool and Head
%% allocation     - receive (nominal) allocation (Ri) and decide how many resources will be appropriated from the pool (Ri_approp)
%% pool_resources - pool's answer to request, with available resources (Ri_real = min(Ri_approp, Pool))
agent(Resources={GTurn, QTurn, PTurn},
      Stats={PCheat, TDemand, TProv, TAlloc, TKept, TGen, Utility, Satisfaction},
      Counts={TurnsPlayed, TurnsCoop, TurnsPunished, TurnsAllocated},
      VoicesParams={Trusts, Neighbours},
      PVPParams={PObs, PForg, UpdMeth, PForgInit}) ->
    receive
        %%start new turn, defining g, q, p and d.
        {new_turn, Situation} ->
            case Situation of
                regular ->
                    %% set availability and need of resources
                    ResourcesMode = aux:lookup_ets(globalvars, resourcesmode),
                    G = turn_resources(ResourcesMode),
                    Q = turn_need(ResourcesMode, G),
                    %% define demand and provision
                    P = provision(G, PCheat),
                    D = demand(Q),
                    pool ! {contribute, P},
                    master ! {agent_facts, {self(), {P, D, G, Q}}},
                    agent({G, Q, P},
                          {PCheat, TDemand+D, TProv+P, TAlloc, TKept+G-P, TGen+G, Utility, Satisfaction},
                          {TurnsPlayed+1.0, TurnsCoop+(if P==G->1.0; P/=G->0.0 end), TurnsPunished, TurnsAllocated},
                          VoicesParams,
                          PVPParams);
                sanctioned ->
                    %% UpPCheat = reinforce(PCheat, 0.1, negative),
                    %% don't contribute with the pool neither send facts to master
                    %% master ! {finished_turn, {Utility}},
                    agent({0.0, 0.0, 0.0},
                          {PCheat, TDemand, TProv, TAlloc, TKept, TGen, Utility, Satisfaction},
                          {TurnsPlayed, TurnsCoop, TurnsPunished+1.0, TurnsAllocated},
                          VoicesParams,
                          PVPParams) %don't update the counts when don't play
            end;
        %%receive allocation from head and decide how much will be requested from common pool
        {allocation, R} ->
            R_approp = appropriate(R),
            pool ! {request, R_approp, self()},
            %%receive the actual resources from pool, depending on the availability
            R_real = receive {pool_resources, R_received} -> R_received end,
            Accrued = R_real + (GTurn - PTurn),
            A = 2, B = 1, C = 3,
            TurnUtility = if Accrued >= QTurn -> A*QTurn + B*(Accrued - QTurn);
                             Accrued < QTurn -> A*Accrued - C*(QTurn - Accrued)
                          end,
            Alpha = Beta = 0.1,
            UpSatisfaction = if (R_real >= QTurn) and (QTurn > 0) -> (1-Alpha)*Satisfaction + Alpha;
                                (R_real == QTurn) and (QTurn == 0) -> Satisfaction; %under sanction round, QTurn is 0, therefore shouldn't update satisf.
                                R_real < QTurn -> (1-Beta)*Satisfaction
                             end,
            {UpPObs, UpPForg} =
                case aux:lookup_ets(globalvars, gamemode) of
                    pvp -> %%only update behaviour if PVP is true
                        if (R_real >= QTurn) and (QTurn > 0) ->
                                update_behaviour(satisfied, PObs, PForg, TurnsPlayed, UpdMeth);
                           (R_real == QTurn) and (QTurn == 0) ->
                                {PObs, PForg}; % in sanctioning rounds, don't update behaviour
                           R_real < QTurn ->
                                update_behaviour(unsatisfied, PObs, PForg, TurnsPlayed, UpdMeth)
                        end;
                    _Other ->
                        {PObs, PForg}
                end,
            ShouldQuit = (aux:lookup_ets(globalvars, quiters)) and (UpSatisfaction =< ?UNSATISF_THRESH),
            if ShouldQuit ->
                    io:format("Quiter! ~p~n", [self()]),
                    master ! {finished_turn, {self(), quit}};
               not ShouldQuit ->
                    master ! {finished_turn, {self(), stay}}
            end,
            agent({0.0, 0.0, 0.0},
                  {PCheat, TDemand, TProv, TAlloc+R_real, TKept, TGen, Utility+TurnUtility, UpSatisfaction},
                  {TurnsPlayed, TurnsCoop, TurnsPunished, TurnsAllocated+(if R_real>0.0 -> 1.0; R_real=<0.0 -> 0.0 end)},
                  VoicesParams,
                  {UpPObs, UpPForg, UpdMeth, PForgInit});
        voices ->
            %% elaborate personal opinion
            Phi = if PCheat == 0.0 -> Satisfaction;
                     PCheat > 0.0 -> 0.0  %Cheaters are always dissatisfied
                  end,
            UpTrusts = trust_assessment(Phi, Neighbours, Trusts),
            %%propagate opinions
            %%(normalization just happens on propagation)
            InformedOpinion = propagate_opinions(degroot, Phi, UpTrusts),
            master ! {informed_opinion, InformedOpinion},
            agent(Resources, Stats, Counts, {UpTrusts, Neighbours}, PVPParams);
        ddj ->
            %% elaborate personal opinion
            Phi = legitimate_claims(TDemand, TProv, TAlloc, TurnsAllocated,
                                    TurnsCoop, Satisfaction, TurnsPlayed),
            master ! {self_opinion, {self(), Phi}},
            UpTrusts = trust_assessment(Phi, Neighbours, Trusts),
            %%propagate opinions
            %%(normalization just happens on propagation)
            PropTrusts = propagate_trust(kurka, UpTrusts),
            master ! {neigh_reputations, PropTrusts},
            agent(Resources, Stats, Counts, {UpTrusts, Neighbours}, PVPParams);
        %%deal with the behaviour of a neighbour , deciding if should be investigated or not
        {judgecase, J, {Pj,_Dj,Gj,_Qj}} ->
            % First decide if neighbour is worth of investigation
            PForgJ = maps:get(J, PForg, PForgInit), %initialize PForg if first decision
            ShouldInvestigate = aux:flip(PObs),
            Decision = case ShouldInvestigate of
                           false ->
                               didntobserve;
                           true ->
                               ShouldForgive = aux:flip(PForgJ),
                               if Pj >= Gj ->
                                       innocent;
                                  (Pj < Gj) and (not ShouldForgive) ->
                                       {punish, J};
                                  (Pj < Gj) and ShouldForgive ->
                                       forgive
                               end
                       end,
            % update PObs, PForg and calculate costs based on decision
            {MonCost, PunCost} = case Decision of
                                     didntobserve -> {0.0, 0.0};
                                     innocent -> {?MONITCOST, 0.0};
                                     {punish, J} -> {?MONITCOST, ?PUNCOST};
                                     forgive -> {?MONITCOST, 0.0}
                                 end,
            {UpPObs, UpPForg} = update_behaviour({Decision, J, PForgJ}, PObs, PForg, TurnsPlayed, UpdMeth),
            master ! {judgedecision, {self(), Decision, MonCost, PunCost}},
            agent(Resources, Stats, Counts, VoicesParams,
                  {UpPObs, UpPForg, UpdMeth, PForgInit});
        {updateneighbours, NewNeighbours} ->
            agent(Resources, Stats, Counts, {Trusts, NewNeighbours}, PVPParams);
        terminate ->
            %% io:format("~w Ut=~.2f Sat=~.2f R=~.2f (A=~.2f, K=~.2f) R/N=~.2f N=~.2f G=~.2f PC=~w~n",
            %%           [self(), Utility, Satisfaction, TAlloc+TKept, TAlloc, TKept, (TAlloc+TKept)/TNeed, TNeed, TGen, PCheat]),
            io:format("| ~w\t| ~w\t|~n", [TurnsCoop, TurnsPunished]),
            master ! {genstats, {PCheat, [Utility, TurnsPlayed, Satisfaction, PObs, TAlloc, TKept]}},
            master ! {pforgstats, {self(), PCheat, PForg}},
            aux:check_unread_msgs(),
            exit(normal)
    end.
%% Aux agent functions

%% define available resources in turn. Modes: fixed, random
turn_resources(fixed) -> 0.5;
turn_resources(random) -> rand:uniform().


%% define resources needed in turn.
turn_need(fixed, _Gi) -> 0.75;
turn_need(random, Gi) -> Gi + rand:uniform() * (1-Gi).


provision(Gi, 0.0) -> Gi;
provision(Gi, PCheat) ->
    case aux:flip(PCheat) of
        true -> Gi*rand:uniform();
        false -> Gi
    end.

demand(Qi) -> Qi.
appropriate(Ri) -> Ri.

%% Voices/DDJ functions
legitimate_claims(TDemand, TProv, TAlloc, PosAlloc, CoopTurns, Satisfaction, NTurns) ->
    %% f1a - average allocation
    F1a = TAlloc / NTurns,
    %% f1b - satisfaction
    F1b = Satisfaction,
    %% f1c - positive allocations
    F1c = PosAlloc / NTurns,
    %% f2 - average demand
    F2 = TDemand / NTurns,
    %% f3 - average provision (decreasing)
    F3 = 1.0 - (TProv / NTurns),
    %% f4 - turns played (decreasing) (not used)
    %% F4 = 1 - NTruns
    %% f5 - social utility (decreasing) (not used)
    %% f6 - social compliancy
    F6 = 1.0 - (CoopTurns / NTurns),
    aux:average([F1a, F1b, F1c, F2, F3, F6]).

trust_assessment(Phi, Neighbours, Trusts) ->
    %% send opinion to neighbours
    aux:broadcast(Neighbours, {init_opinion, {self(), Phi}}),
    %% listen from neighbours
    NeighboursOpinions = maps:from_list(aux:accumulate_data(length(Neighbours), init_opinion)),
    %% compute opinion affinity to each neighbour
    TurnAffinities = calculate_affinities(Phi, NeighboursOpinions),
    update_trusts(TurnAffinities, Trusts).

calculate_affinities(Phi, NeighboursOpinions) ->
    Neighbours = maps:keys(NeighboursOpinions),
    SumOpinions = Phi + lists:sum(maps:values(NeighboursOpinions)),
    NOpinions = 1 + length(Neighbours),
    {Thresh, K} = case aux:lookup_ets(globalvars, gamemode) of
                      voices -> {0.2, 20}; %voices case
                      ddj -> {0.25, 15} %ddj case
                  end,

    maps:map(
      fun(_N, Oj) ->
              ReferenceOpinion = (SumOpinions-Oj)/(NOpinions-1),
              aux:logistic(abs(ReferenceOpinion-Oj), K, Thresh)
      end,
      NeighboursOpinions).

update_trusts(TurnAffinities, OldTrusts) ->
    ReinfRate = 0.1,
    NeighTrusts = maps:map(
                    fun(N, Affinity) ->
                            %% initial trust is 1.0
                            OldTrust = maps:get(N, OldTrusts, 1.0),
                            (1-ReinfRate)*OldTrust + ReinfRate*Affinity
                    end,
                    TurnAffinities),
    maps:put(self(), 1.0, NeighTrusts). %%add self trust (always 1.0) on trusts map

propagate_opinions(degroot, InitOpinion, Trusts) ->
    NIterations = 100,
    TrustsSum = maps:fold(fun(_,T,Acc) -> Acc+T end, 0.0, Trusts),
    NormTrusts = maps:map(fun(_,V) -> V/TrustsSum end, Trusts),
    Neighbours = lists:delete(self(), maps:keys(Trusts)),
    degroot(NIterations, InitOpinion, NormTrusts, Neighbours).

degroot(0, CurOpinion, _, _) -> CurOpinion;

degroot(NIterations, CurOpinion, NormTrusts, Neighbours) ->
    %% Exchange opinions with neighbours
    MessageId = list_to_atom("opinion_" ++ integer_to_list(NIterations)),
    aux:broadcast(Neighbours, {MessageId, {self(), CurOpinion}}),
    CurNeighOpinions = aux:accumulate_data(length(Neighbours), MessageId),
    %% Formulate new opinion averaging trust
    NewOpinion = lists:foldl(fun({N,O}, OAcc) -> OAcc + maps:get(N, NormTrusts)*O end,
                             0.0,
                             [{self(), CurOpinion}|CurNeighOpinions]),
    degroot(NIterations-1, NewOpinion, NormTrusts, Neighbours).

propagate_trust(kurka, Trusts) ->
    NIterations = 50,
    Neighbours = lists:delete(self(), maps:keys(Trusts)),
    maps:to_list(kurka(NIterations, Trusts, Neighbours)).

kurka(0, Trusts, _) -> Trusts;

kurka(NIterations, Trusts, Neighbours) ->
    %% Exchange opinions with neighbours
    MessageId = list_to_atom("opinion_" ++ integer_to_list(NIterations)),
    aux:broadcast(Neighbours, {MessageId, {self(), Trusts}}),
    NeighTrusts = maps:from_list(aux:accumulate_data(length(Neighbours), MessageId)),
    %% Formulate new opinion averaging trust
    PropTrusts = maps:from_list(
                   lists:map(
                     fun(J) ->
                             Inter = maps:get(J, NeighTrusts),
                             NeighsJ = maps:keys(Inter),
                             %% NeighsJ = maps:keys(maps:get(J, NeighTrusts)),
                             CommonNeighs = (Neighbours -- (Neighbours -- NeighsJ)) -- [J] ++ [self()],
                             NormFactor = lists:foldl(fun(K, Acc) -> Acc+maps:get(K, Trusts) end,
                                                      0.0,
                                                      CommonNeighs),
                             UpTrustJ = lists:foldl(
                                          fun(K, Acc) ->
                                                  TrustsK = maps:get(K, NeighTrusts),
                                                  Acc+((maps:get(K, Trusts)*maps:get(J, TrustsK))/NormFactor)
                                          end,
                                          0.0,
                                          CommonNeighs),
                             {J, UpTrustJ}
                     end,
                     Neighbours)),
    kurka(NIterations-1, PropTrusts, Neighbours).


%% PVP functions
update_behaviour(Decision, PObs, PForg, TurnsPlayed, reinforcement) ->
    NormFactor = 1 + trunc(TurnsPlayed/10),
    ReinfStep = 0.1/NormFactor,
    case Decision of
        {didntobserve, J, PForgJ} ->
            {PObs,
             maps:put(J, PForgJ, PForg)}; % value is not changed, but is inserted in case used the default
        {innocent, J, PForgJ} ->
            {reinforce(PObs, 3*ReinfStep, negative), %decrease monitoring if couldn't find transgression
             maps:put(J, reinforce(PForgJ, ReinfStep, positive), PForg)}; %increase the chance of forgiveness
        {{punish, J}, J, PForgJ} ->
            {reinforce(PObs, ReinfStep, positive), %increase monitoring when find problems
             maps:put(J, reinforce(PForgJ, 4*ReinfStep, negative), PForg)}; %also decrease forgiveness
        {forgive, J, PForgJ} ->
            {reinforce(PObs, ReinfStep, positive),
             maps:put(J, reinforce(PForgJ, 2*ReinfStep, negative), PForg)};
        satisfied -> {PObs, PForg};
        unsatisfied -> {reinforce(PObs, ReinfStep, positive), PForg}
    end;

update_behaviour(Decision, PObs, PForg, _TurnsPlayed, fixed) ->
    case Decision of
        {_individual, J, PForgJ} ->
            {PObs,
             maps:put(J, PForgJ, PForg)};
        _general -> {PObs, PForg}
    end.


reinforce(OriginalValue, StepSize, Direction) ->
    case Direction of
        positive ->
            (1-StepSize)*OriginalValue + StepSize;
        negative ->
            (1-StepSize)*OriginalValue
    end.

%% Actor responsible to initiating turns and counting turns.
master(AllAgents, _Edges, _CurrentSanctions, DeletedAgents, 0) ->
    NAgents = length(AllAgents),
    %% kill all processes.
    aux:broadcast(AllAgents, terminate),
    GenStats = aux:accumulate_data(NAgents, genstats),
    PForgData = aux:accumulate_data(NAgents, pforgstats),
    case aux:lookup_ets(globalvars, verbose) of
        true ->
            split_and_print_stats(GenStats),
            print_pforg_table(PForgData);
        false ->
            pass
    end,
    pool ! terminate,
    timer:sleep(200),
    RoundUtilities = get_utility_per_pcheat(GenStats),
    io:format("Deleted Agents: ~p~n", [DeletedAgents]),
    aux:check_unread_msgs(),
    main ! {endgame, RoundUtilities};

master(ActiveAgents, Edges, CurrentSanctions, DeletedAgents, NTurns) ->
    NAgents = length(ActiveAgents),
    %% turn execution
    %% 1. start new turn, where demands and provision are executed
    {PlayingAgents, SanctionedAgents} = lists:partition(
                                         fun(A) -> maps:get(A, CurrentSanctions#recsanc.sanctcount) == 0 end,
                                         ActiveAgents),
    aux:broadcast(PlayingAgents, {new_turn, regular}),
    aux:broadcast(SanctionedAgents, {new_turn, sanctioned}),
    %% 2. receive all demands (and provisions) from agents
    AllAgentsFacts = maps:from_list(aux:accumulate_data(length(PlayingAgents), agent_facts)),
    %% 3. Allocate resources to agents
    compute_allocations(AllAgentsFacts, ActiveAgents, CurrentSanctions),
    %% 4. Get people's opinions using voices of justice mechanism
    case aux:lookup_ets(globalvars, gamemode) of
        voices ->
            aux:broadcast(ActiveAgents, voices),
            VoicesOutput = aux:accumulate_data(NAgents, informed_opinion),
            io:format("~p: ~p~n", [NTurns, aux:average(VoicesOutput)]);
        _Other ->
           pass
    end,
    %% 5. Apply sanctions
    UpSanctions = case aux:lookup_ets(globalvars, gamemode) of
                      pvp ->
                          compute_sanctions(AllAgentsFacts, CurrentSanctions, Edges);
                      _ ->
                          CurrentSanctions
                  end,
    %% 6. Sync turn
    FinishedAgents = aux:accumulate_data(NAgents, finished_turn),
    {Quiters, UpEdges} = treat_quiters(FinishedAgents, ActiveAgents, Edges),
    master(ActiveAgents--Quiters, UpEdges, UpSanctions, DeletedAgents++Quiters, NTurns-1).

compute_allocations(AllAgentsFacts, AllAgents, Sanctions) ->
    AllocMethod = case aux:lookup_ets(globalvars, gamemode) of
                      voices -> random;
                      ddj -> self_organised;
                      pvp -> random
                  end,
    AllocOrder = allocation_order(AllocMethod, AllAgents),
    Pool = lists:foldl(fun({Pi,_Di,_Gi,_Qi}, P) -> P + Pi end,
                       0.0,
                       maps:values(AllAgentsFacts)),
    SanctionedAgents = maps:keys(maps:filter(fun(_K,V) -> V > 0 end, Sanctions#recsanc.sanctcount)),
    alloc(AllocOrder, AllAgentsFacts, Pool, SanctionedAgents, Sanctions).

allocation_order(random, Agents) -> aux:shuffle(Agents);

%% allocation case when there is just a single agent in the game (therefore no reputation)
allocation_order(self_organised, [SingleAgent]) -> [SingleAgent];

allocation_order(self_organised, Agents) ->
    %% Ask for agents' opinions
    aux:broadcast(Agents, ddj),
    IndividualOpinions = aux:accumulate_data(length(Agents), self_opinion),
    %% Get agents reputations
    TrustsList = lists:flatten(aux:accumulate_data(length(Agents), neigh_reputations)),
    Reputations = aux:split_per_key(TrustsList),

    %% Sentiment = (1-Opinion)*Reputation
    Sentiments = lists:map(fun({Pid, Phi}) -> {Pid, (1-Phi)*aux:average(maps:get(Pid, Reputations))} end,
                           IndividualOpinions),
    %% sort agents by sentiment and return their Pids
    lists:map(fun ({Pid, _}) -> Pid end, lists:keysort(2, aux:shuffle(Sentiments))).


alloc([H|T], AllAgentsFacts, P, SanctionedAgents, Sanctions) ->
    case lists:member(H, SanctionedAgents) of
      %% if not in sanctioned, give what was requested (if enough resources)
      false ->
            {_Pi,Di,_Gi,_Qi} = maps:get(H, AllAgentsFacts),
            Alloc_i = lists:min([Di, P]),
            Disc = (maps:get(H, Sanctions#recsanc.moncost, 0.0) +
                        maps:get(H, Sanctions#recsanc.puncost, 0.0)),
            Alloc_i_disc = Alloc_i - Disc,
            H ! {allocation, Alloc_i_disc},
            alloc(T, AllAgentsFacts, P-Alloc_i, SanctionedAgents, Sanctions);

        %% don't give anything to sanctioned agents
        true ->
            H ! {allocation, 0.0},
            alloc(T, AllAgentsFacts, P, SanctionedAgents, Sanctions)
    end;

alloc([], _, _, _, _) -> ok.

compute_sanctions(AllAgentsFacts, CurrentSanctions, Edges) ->
    {SanctRound, MonCosts, PunCosts} = round_sanctions(AllAgentsFacts, CurrentSanctions, Edges),
    UpSanctionsCounts = update_sanctions(CurrentSanctions, SanctRound),
    #recsanc{sanctcount=UpSanctionsCounts, moncost=MonCosts, puncost=PunCosts}.


round_sanctions(AllAgentsFacts, CurSanctions, Edges) ->
    %% just judge agents that are active
    LiveEdges = [E || E={_I,J} <- Edges, maps:get(J, CurSanctions#recsanc.sanctcount) == 0],
    %% give each agent another agent to be judged
    lists:foreach(
      fun({J,C}) -> J ! {judgecase, C, maps:get(C, AllAgentsFacts)} end, %send culprits to agents decide what to do with them.
      LiveEdges),
    Decisions = aux:accumulate_data(length(LiveEdges), judgedecision),
    {SetSanctioned, MonCosts, PunCosts} =
        lists:foldl(
          fun({J,Decision,MC,PC}, {AccSanctioned, MCosts, PCosts}) ->
                  UpMCosts = maps:put(J, maps:get(J, MCosts, 0.0)+MC, MCosts),
                  UpPCosts = maps:put(J, maps:get(J, PCosts, 0.0)+PC, PCosts),
                  UpAccSanctioned = case Decision of
                                        {punish, C} -> sets:add_element(C, AccSanctioned);
                                        _ -> AccSanctioned %%don't change anything
                                    end,
                  {UpAccSanctioned, UpMCosts, UpPCosts}
          end,
          {sets:new(), maps:new(), maps:new()},
          Decisions),
    {sets:to_list(SetSanctioned), MonCosts, PunCosts}.


update_sanctions(CurrentSanctions, SanctionedInRound) ->
    %% Decrease the round count of agents
    SanctionsBeforeRound = maps:map(
                            fun
                                (_K,V) when V>0 -> V-1;
                                (_K,0) -> 0
                            end,
                            CurrentSanctions#recsanc.sanctcount),
    lists:foldl(
      fun(SancAg, AccUpSanctions) -> maps:update(SancAg, ?TURNS_PENALTY, AccUpSanctions) end,
      SanctionsBeforeRound,
      SanctionedInRound).

treat_quiters(FinishedAgentsInfo, ActiveAgents, CurEdges) ->
    Quiters = lists:filtermap(
                fun({Pid, Decision}) ->
                        case Decision of quit -> {true, Pid}; stay -> false end
                end,
                FinishedAgentsInfo),
    UpEdges = if length(Quiters) > 0 ->
                      RecEdges = reconnect_edges(CurEdges, Quiters),
                      Ag2Nei = aux:split_per_key(RecEdges),
                      MissingAg = ActiveAgents -- maps:keys(Ag2Nei),
                      UpNeighs = maps:to_list(maps:map(fun(_,N) -> lists:sort(N) end, Ag2Nei))
                          ++ lists:map(fun(Ag) -> {Ag, []} end, MissingAg),
                      lists:foreach(fun({Pid, UNeigh}) -> Pid ! {updateneighbours, UNeigh} end,
                                    UpNeighs),
                      RecEdges;
                 length(Quiters) == 0 ->
                      CurEdges
              end,
    {Quiters, UpEdges}.

reconnect_edges(Edges, Quiters) ->
    %% treat each agent removal independently
    lists:foldl(fun(Ag, CurEdges) -> remove_agent(Ag, CurEdges) end, Edges, Quiters).

remove_agent(Ag, Edges) ->
    NeiAg = lists:filtermap(fun({I,J}) ->
                                    case I == Ag of true -> {true, J};
                                              false -> false end end, Edges),
    ReconnectedEdges = reconnect_neighbours(Ag, NeiAg, Edges),
    %% Remove edges starting from agent to be removed, if still any left
    %% (this only happens when network is small and agents can't reconnect to new neighbours, I think)
    lists:filter(fun({I,_J}) -> I /= Ag end, ReconnectedEdges).

%% return current edges when there is no neighbour left to be reconnected
reconnect_neighbours(_Ag, [], CurEdges) -> CurEdges;

reconnect_neighbours(Ag, NeiToReconnect, CurEdges) ->
    %% find closest neighbours: defined as agents which
    %% neighbours intersection is equal to one
    NeighsMap = aux:split_per_key(CurEdges),
    NeiAg = maps:get(Ag, NeighsMap),
    Closest = lists:filter(
                fun(N1) ->
                        NeiN1 = maps:get(N1, NeighsMap),
                        Complement = (NeiAg -- NeiN1) -- [N1],
                        length(Complement) < 2
                end,
                NeiToReconnect),

    ClosestNewTargets = lists:flatmap(
                             fun(C) -> NeiC = maps:get(C, NeighsMap), (NeiAg--NeiC)--[C] end,
                             Closest),
    if length(Closest) == 0 ->
            erlang:error(io:format("~p~n~p~n~p~n~p~n~p~n~p~n", [Ag, NeiToReconnect, CurEdges, NeighsMap, NeiAg, ClosestNewTargets]));
       true -> pass
    end,
    ReconnectClosest = fun({I,J}) ->
                               %% find old connection of closest with Ag or Ag to reconnected Agent
                               case {((lists:member(I, Closest)) and (J==Ag)),
                                     ((I==Ag) and (lists:member(J, ClosestNewTargets)))} of
                                   %% reconnect with closest new nember
                                   {true, false} ->
                                       NeiN1 = maps:get(I, NeighsMap),
                                       NewNeighbour = (NeiAg--NeiN1)--[I],
                                       case NewNeighbour of
                                           [N2] -> {true, {I, N2}};
                                           %% don't create new connection
                                           %% when NewNeigh is empty
                                           [] -> false
                                       end;
                                   %% remove fixed connection from Ag neighbourhood
                                   {false, true} -> false;
                                   %% keep edge if not target
                                   {false, false} -> true
                               end
                       end,
    UpEdges = lists:filtermap(ReconnectClosest, CurEdges),
    reconnect_neighbours(Ag, NeiToReconnect--Closest, UpEdges).



split_and_print_stats(AllStats) ->
    % first split agents by their PCheat
    StatPerPCheat = aux:split_per_key(AllStats),
    %% io:format("~n~n~nFinal Stats:~nPCheat\tUtility\t\t\tSatisfaction\tTAlloc~n"),
    %% %% print average and std for each PCheat
    lists:foreach(
      fun(PCheat) ->
              %% print title
              io:format("| ~.2f\t|", [PCheat]),
              %% print avg and std of values separated by tab
              lists:foreach(
                fun
                    (Dist=[DH|_DT]) when is_map(DH) ->
                        StatPerK = aux:split_per_key(lists:flatmap(fun maps:to_list/1, Dist)),
                        lists:foreach(
                          fun({K, D}) ->
                                  io:format(" ~w: ~.2f", [K, lists:sum(D)])
                          end,
                          maps:to_list(StatPerK)),
                        io:format("|");
                    (Dist) ->
                        io:format(" ~.2f\t|", [lists:sum(Dist)])
                end,
                aux:zipn(maps:get(PCheat, StatPerPCheat))),
              %% trailing new line
              io:format("~n", [])
              %% %%print all results for debugging
              %% lists:foreach(
              %%   fun(Dist) ->
              %%           io:format("~p~n",
              %%                     [Dist])
              %%   end,
              %%   aux:zipn(maps:get(PCheat, StatPerPCheat))),
              %% %% trailing new line
              %% io:format("~n", [])
      end,
      maps:keys(StatPerPCheat)).

%% PForgData: {Pid, PCheat, PForg}
print_pforg_table(PForgData) ->
    io:format("PFD:~n"),
    %% group Pids per PCheat
    %% LookupPCheat = maps:from_list(lists:map(fun({I,C,_F}) -> {I,C} end, PForgData)),
    IdsPerPCheat = aux:split_per_key(lists:map(fun({I,C,_F}) -> {C, I} end, PForgData)),
    JudgementsPerId = aux:split_per_key(
                        lists:flatmap(
                          fun({_I,C,F}) -> [{IdJ, {C, PForgJ}} ||
                                               {IdJ, PForgJ} <- maps:to_list(F)] end,
                          PForgData)),
    lists:foreach(
      fun({PCheatT, TIds}) ->
              io:format("| ~w\t|", [PCheatT]),
              PForgPerPCheat = aux:split_per_key(
                                 lists:append([maps:get(IdJ, JudgementsPerId, []) || IdJ <- TIds])
                                ),
              lists:foreach(
                fun({K,D}) ->
                        io:format(" ~.2f: ~.2f+-~.2f", [K, aux:average(D), aux:std(D)])
                end,
                maps:to_list(PForgPerPCheat)),
              io:format("|~n")
      end,
      maps:to_list(IdsPerPCheat)
     ).

get_utility_per_pcheat(AllStats) ->
    StatPerPCheat = aux:split_per_key(AllStats),
    lists:map(
      fun({_PCheat, PCheatStats}) ->
              [Dist|_] = aux:zipn(PCheatStats),
              lists:sum(Dist)
      end,
      lists:sort(maps:to_list(StatPerPCheat))
     ).

init_agent(PCheat, PObsInit, LearningType, PForgInit) ->
    receive
        {neighbours, Neighbours} ->
            InitTrusts = maps:from_list(lists:zip(Neighbours, lists:duplicate(length(Neighbours), 1.0))),
            agent({0.0, 0.0, 0.0}, %Resources
                  {PCheat, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.5}, %Stats
                  {0.0, 0.0, 0.0, 0.0}, %Counts
                  {InitTrusts, Neighbours}, %VoicesParams
                  {PObsInit, #{}, LearningType, PForgInit} %PVPParams
                 )
    end.



%% Main execution function.
start( ) ->
    %% global variables trick
    ets:new(globalvars, [set, public, named_table]),
    ets:insert(globalvars, {verbose, ?VERBOSE}),
    ets:insert(globalvars, {gamemode, ?GAMEMODE}),
    ets:insert(globalvars, {quiters, ?QUITERS}),
    ets:insert(globalvars, {resourcesmode, ?RESOURCESMODE}),

    io:format("PENALTY: ~w MONITCOST: ~w PUNCOST: ~w~n",
              [?TURNS_PENALTY, ?MONITCOST, ?PUNCOST]),
    register(main, self()),
    NAgents = 10,
    %% exp1a(NAgents, 6),
    %% exp1b(NAgents, 2),
    %% exp2(NAgents, 2),
    %% exp3(),
    PC = 0.2,
    PCheats = lists:duplicate(NAgents, PC),
    %% PCheats = llists:duplicate(NAgents div 3, 0.01) ++ lists:duplicate(2*NAgents div 3, 0.2),
    NNeigh = 4,
    PO = 0.5, PF = 0.5,
    _Result = sample_run(PCheats, fixed, {kneighbours, NNeigh}, PO, PF),

    exit(normal).

sample_run(PCheats, LearningType, EdgesType, PObsInit, PForgInit) ->
    NTurns = 1000,
    register(pool, spawn(km, pool, [0.0])),
    InitAgent = fun(PCheat) -> spawn(km, init_agent, [PCheat, PObsInit, LearningType, PForgInit]) end,
    %% AllAgents = aux:shuffle(lists:map(InitAgent, PCheats)),
    AllAgents = lists:map(InitAgent, PCheats),
    Edges = create_edges(AllAgents, EdgesType),
    io:format("Agents: ~p~nEdges: ~p~n", [AllAgents, Edges]),
    Neighs = lists:keymap(fun lists:sort/1, 2, maps:to_list(aux:split_per_key(Edges))),
    lists:foreach(fun({Pid, Neigh}) -> Pid ! {neighbours, Neigh} end, Neighs),

    InitSanctionsCount = maps:from_list([{A, 0} || A <- AllAgents]),
    InitSanctions = #recsanc{sanctcount=InitSanctionsCount},
    Master = spawn(km, master, [AllAgents, Edges, InitSanctions, [], NTurns]),
    register(master, Master),
    receive
        {endgame, Results} -> Results
    after 1000*30 ->
            io:format("Timeout function~n"),
            exit(whereis(pool), kill),
            exit(Master, kill),
            lists:foreach(fun(Pid) -> exit(Pid, kill) end, AllAgents),
            erlang:error(io:format("Timeout! iteration took to long to finish!"))
    end. %block execution until game finishes

create_edges(AllAgents, {bipartite, NNEIGH}) ->
    {Compliant, NonCompliant} = lists:split(length(AllAgents) div 2, AllAgents),
    pairs1ton(NNEIGH, Compliant, NonCompliant);

create_edges(AllAgents, {kneighbours, NNEIGH}) ->
    aux:kneighbours(aux:shuffle(AllAgents), NNEIGH).

%% create n pairs from list A to list B, rotating list B n times
pairs1ton(N, A, B) -> pairs1ton(N, A, B, []).
pairs1ton(0, _, _, Acc) -> Acc;
pairs1ton(N, A, B=[HB|TB], Acc) ->
    pairs1ton(N-1, A, TB++[HB], lists:zip(A,B)++Acc).

